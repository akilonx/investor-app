import React, { useState } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { useQuery, useMutation } from '@apollo/react-hooks'
import TableHalf from '../common/tableHalf'
import TransactionDialog from './transactionDialog'
import { GET_ORDERS, REMOVE_ORDER } from '../graphql/order'
import { GET_PAYROLSTAFFS } from '../graphql/payroll'
import ConfirmationDialog from '../common/confirmationDialog'
import Loading from '../common/loading'
import RefreshIcon from '@material-ui/icons/Refresh'
import DatepickerField from '../common/datepickerField'

import PayrollDialog from './officestaffPayrollDialog'
import PayrollDialogDetail from './officestaffPayrollDetailsDialog'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  displayDiv: {
    background: theme.palette.background.paper,
    padding: '20px',
    minHeight: '340px',
  },
  newButton: {
    marginBottom: 10,
  },
}))

const tableHead = [
  {
    id: 'id',
    numeric: false,
    disablePadding: false,
    label: 'ID',
  },
  {
    id: 'StaffID',
    label: 'Staff',
  },
  {
    id: 'InvoiceAmount',
    label: 'Sales',
    currency: true,
  },
]

export default function Leave() {
  const classes = useStyles()
  const [selected, setSelected] = useState([])
  const [open, setOpen] = useState(false)
  const [confirm, setConfirm] = useState(false)
  const [payroll, setPayroll] = useState()
  const [from, setFrom] = useState()
  const [to, setTo] = useState()

  const {
    loading,
    data: { payrollstaffs } = { payrollstaffs: [] },
    refetch: refetchPayrollStaff,
  } = useQuery(GET_PAYROLSTAFFS, { skip: !from || !to })

  const handleClickSearch = () => {
    if (from && to) refetchPayrollStaff({ FromDate: from, ToDate: to })
  }

  /* if (loading) return <Loading /> */

  return (
    <div className={classes.root}>
      {/* <Button
        variant="contained"
        disableElevation
        classes={{ root: classes.newButton }}
        color="primary"
        onClick={handleClickOpen}
      >
        New
      </Button> */}
      <Button
        variant="outlined"
        classes={{ root: classes.newButton }}
        color="primary"
        startIcon={<RefreshIcon />}
        onClick={() => {
          refetchPayrollStaff({ FromDate: from, ToDate: to })
        }}
      >
        Refresh
      </Button>
      <Grid container spacing={3}>
        <Grid item md={3} xs={12}>
          <DatepickerField
            name="From"
            label="From"
            value={from ? from : null}
            fullWidth
            onChange={(e, value) => {
              setFrom(value)
            }}
            autoComplete="off"
          />
        </Grid>
        <Grid item md={3} xs={12}>
          <DatepickerField
            name="To"
            label="To"
            value={to ? to : null}
            fullWidth
            onChange={(e, value) => {
              setTo(value)
            }}
            autoComplete="off"
          />
        </Grid>

        <Grid item md={3} xs={12}>
          <Button
            variant="contained"
            disableElevation
            classes={{ root: classes.newButton }}
            color="primary"
            onClick={handleClickSearch}
          >
            Search
          </Button>
        </Grid>
      </Grid>

      <Grid container spacing={0}>
        <Grid item xs={12} sm={12}>
          <PayrollDialogDetail
            key={+new Date() + Math.random()}
            from={from}
            to={to}
            data={payroll}
            setPayroll={setPayroll}
            setSelected={setSelected}
            open={open}
            setOpen={setOpen}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TableHalf
            clickOnSelect={true}
            hidePagination={true}
            hideChange={true}
            hideDelete={true}
            setConfirm={setConfirm}
            selected={selected}
            setSelected={setSelected}
            tableState={setPayroll}
            tableData={payrollstaffs}
            setOpen={setOpen}
            tableHead={tableHead}
          ></TableHalf>
        </Grid>
        {/* <Grid item xs={12} sm={6}>
          {order && order.id && (
            <div className={classes.displayDiv}>
              <ProductDisplay data={order} />
            </div>
          )}
        </Grid> */}
      </Grid>
      {/*  <pre>{JSON.stringify(orders, null, 4)}</pre> */}
    </div>
  )
}
