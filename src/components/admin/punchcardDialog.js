import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import { useMutation, useQuery } from '@apollo/react-hooks'
import Divider from '@material-ui/core/Divider'
import CurrencyFormat from 'react-currency-format'

import {
  UPDATE_PUNCHCARD,
  PUNCHCARD_HISTORY,
  INSERT_PUNCHCARD,
  REMOVE_PUNCHCARD,
} from '../graphql/punchcard'

import TextField from '@material-ui/core/TextField'
import ConfirmationDialog from '../common/confirmationDialog'
import useForm from 'react-hook-form'
import Loading from '../common/loading'
import axios from 'axios'
import DeleteIcon from '@material-ui/icons/Delete'
import Cookies from 'js-cookie'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import ImageIcon from '@material-ui/icons/Image'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import { Editor } from '@tinymce/tinymce-react'

import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import DatepickerField from '../common/datepickerField'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  rootList: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  listimage: {
    width: 100,
    paddingRight: 10,
  },
  h5: {
    marginBottom: 5,
    marginTop: 5,
  },
  big: {
    width: 800,
  },
  imagedialog: {
    boxShadow: '0 8px 6px -6px black',
    position: 'static',
    left: '20%',
    top: '10%',
    zIndex: 9999,
  },
  dialogPaper: {
    background: theme.palette.primary.paper,
    overflowY: 'visible',
  },
  dialogTitle: {
    background: theme.palette.primary.backgroundColor,
    color: theme.palette.primary.main,
  },
  dialogContent: {
    background: theme.palette.primary.backgroundColor,
    overflowY: 'visible',
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.primary.main,
  },
  dialogActions: {
    padding: theme.spacing(3),
  },
  backDrop: { backgroundColor: 'transparent' },
  dividerroot: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  formControl: {
    marginTop: 5,
    minWidth: 250,
  },
}))

export default function ProductDialog(props) {
  const classes = useStyles()
  const { handleSubmit, register, errors, setValue } = useForm()

  const [confirm, setConfirm] = useState(false)
  const [intime, setIntime] = useState()
  const [outtime, setOuttime] = useState()
  const addCache = (cache, { data }) => {
    const old = cache.readQuery({
      query: PUNCHCARD_HISTORY,
    })
    const latest = data.insertpunchcard
    cache.writeQuery({
      query: PUNCHCARD_HISTORY,
      data: { punchcardhistory: [latest, ...old.punchcardhistory] },
    })
    props.setOpen(false)
    props.setSelected([])
    props.setPunchcard()
  }

  const updateCache = (cache, { data }) => {
    const old = cache.readQuery({
      query: PUNCHCARD_HISTORY,
    })

    const latest = data.updatepunchcard

    const foundIndex = old.punchcardhistory.findIndex(
      (item) => item.id === latest.id
    )
    old.punchcardhistory.splice(foundIndex, 1, latest)
    cache.writeQuery({
      query: PUNCHCARD_HISTORY,
      data: { punchcardhistory: [...old.punchcardhistory] },
    })

    props.setOpen(false)
    props.setSelected([])
    props.setPunchcard()
  }

  const deleteCache = (cache, { data }) => {
    const old = cache.readQuery({
      query: PUNCHCARD_HISTORY,
    })
    if (data.removepunchcard) {
      const latest = old.punchcardhistory.filter(
        (item) => item.id !== props.data.id
      )
      cache.writeQuery({
        query: PUNCHCARD_HISTORY,
        data: { punchcardhistory: [...latest] },
      })

      props.setOpen(false)
      props.setSelected([])
      props.setPunchcard()
    }
  }

  const [insertPunchcard] = useMutation(INSERT_PUNCHCARD, { update: addCache })
  const [updatePunchcard] = useMutation(UPDATE_PUNCHCARD, {
    update: updateCache,
  })
  const [removepunchcard] = useMutation(REMOVE_PUNCHCARD, {
    update: deleteCache,
  })
  const handleClickDelete = () => {
    removepunchcard({
      variables: {
        id: props.data.id,
      },
    })
    setConfirm(false)
  }

  useEffect(() => {
    if (!props.data) return
  }, [props])

  const onSubmit = (values) => {
    if (props.data && props.data.id) {
      updatePunchcard({
        variables: {
          id: props.data.id,
          Intime: values.Intime,
          Outtime: values.Outtime,
        },
      })
    } else {
      insertPunchcard({
        variables: {
          Intime: values.Intime,
          Outtime: values.Outtime,
        },
      })
    }
  }

  return (
    <React.Fragment>
      <ConfirmationDialog
        action={handleClickDelete}
        confirm={confirm}
        setConfirm={setConfirm}
        message="Continue remove PunchCard?"
        okButton="Yes"
        title="Continue remove"
      />
      <React.Fragment>
        <Dialog
          fullWidth={true}
          maxWidth="sm"
          scroll="body"
          open={props.open}
          onClose={() => {
            props.setOpen(false)
            props.setSelected([])
          }}
          disableBackdropClick={false}
          classes={{ paper: classes.dialogPaper }}
          aria-labelledby="order-dialog"
        >
          <React.Fragment>
            <DialogTitle className={classes.dialogTitle} id="order-dialog">
              PunchCard Details
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={() => {
                  props.setOpen(false)
                  props.setSelected([])
                }}
              >
                <CloseIcon />
              </IconButton>
            </DialogTitle>
            <form onSubmit={handleSubmit(onSubmit)}>
              <DialogContent className={classes.dialogContent}>
                <Grid container spacing={3}>
                  <Grid item md={6} xs={12}>
                    <TextField
                      name="Intime"
                      label="Intime"
                      margin="dense"
                      fullWidth
                      defaultValue={props.data && props.data.Intime}
                      style={{ width: 120 }}
                      autoComplete="off"
                      inputProps={{ min: '0', step: 'any' }}
                      inputRef={register({ required: 'Required' })}
                      error={errors.Intime}
                      helperText={errors.Intime && errors.Intime.message}
                    />
                  </Grid>
                  <Grid item md={6} xs={12}>
                    <TextField
                      name="Outtime"
                      label="Outtime"
                      margin="dense"
                      fullWidth
                      defaultValue={props.data && props.data.Outtime}
                      style={{ width: 120 }}
                      autoComplete="off"
                      inputProps={{ min: '0', step: 'any' }}
                      inputRef={register({ required: 'Required' })}
                      error={errors.Outtime}
                      helperText={errors.Outtime && errors.Outtime.message}
                    />
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions className={classes.dialogActions}>
                <IconButton
                  style={{ marginRight: 10 }}
                  onClick={() => {
                    setConfirm(true)
                  }}
                  edge="end"
                  aria-label="comments"
                >
                  <DeleteIcon />
                </IconButton>
                <Button
                  variant="contained"
                  disableElevation
                  color="primary"
                  type="submit"
                >
                  Save
                </Button>
              </DialogActions>
            </form>
          </React.Fragment>
        </Dialog>
      </React.Fragment>
    </React.Fragment>
  )
}
