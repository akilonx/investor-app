import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import TextFieldDisplay from '../common/textFieldDisplay'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import PrintIcon from '@material-ui/icons/Print'
import EditIcon from '@material-ui/icons/Edit'
import { Divider } from '@material-ui/core'
import {
  ColorPicker,
  ColorPalette,
  ColorInput,
  ColorBox,
} from 'material-ui-color'
import { GET_SHOPCONFIG, UPDATE_CONFIG } from '../graphql/config'
import { useMutation } from '@apollo/react-hooks'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  displayDiv: {
    background: theme.palette.background.paper,
    minHeight: '340px',
    margin: 20,
    marginTop: 30,
  },
  small: {
    width: 300,
  },
  big: {
    width: 800,
  },
  dialog: {
    boxShadow: '0 8px 6px -6px black',
    position: 'static',
    left: '20%',
    top: '10%',
  },
  table: {
    padding: 0,
  },
}))

const ShopSettingsDisplay = (props) => {
  const classes = useStyles()
  const [dbhost, setDbhost] = useState('spa2')
  const [color, setColor] = useState('#000')
  //const [dbhost, setDbhost] = useState('storedemo1')

  const updateCache = (cache, { data }) => {
    props.refetch()
    props.setTheme({
      palette: {
        primary: {
          dark: `#${color.hex}`,
          main: `#${color.hex}`,
          paper: '#f5f5f5',
          contrastText: '#f3f3f3',
        },
        secondary: {
          main: `#${color.hex}`,
        },
        background: {
          default: '#efefef',
        },
        type: 'light', // Switching the dark mode on is a single property value change.
      },
      typography: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        fontSize: 15,
        fontWeightLight: 300,
        fontWeightRegular: 400,
        fontWeightMedium: 500,
      },
    })
  }

  const [updateConfig] = useMutation(UPDATE_CONFIG, {
    update: updateCache,
  })

  useEffect(() => {
    setColor(props.data.COLORPRIMARY)
  }, [props])

  return (
    <React.Fragment>
      <Grid container spacing={5}>
        <Grid item spacing={5} md={6} xs={12}>
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMSHO')
                      props.setFormValue(props.data.COMSHO)
                      props.setOpen(true)
                      props.setFormTitle('Shop Title')
                      props.setFormPlaceholder('eg. Ecommerce Company')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Shop Title"
            value={props.data.COMSHO}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMNAM')
                      props.setFormValue(props.data.COMNAM)
                      props.setOpen(true)
                      props.setFormTitle('Company Name')
                      props.setFormPlaceholder('eg. Ecommerce Company Sdn Bhd')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Name"
            value={props.data.COMNAM}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMADD')
                      props.setFormValue(props.data.COMADD)
                      props.setOpen(true)
                      props.setFormTitle('Company Address')
                      props.setFormPlaceholder('')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            multiline
            label="Company Address"
            value={`${props.data.COMADD}`}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMREG')
                      props.setFormValue(props.data.COMREG)
                      props.setOpen(true)
                      props.setFormTitle('Company Registration No')
                      props.setFormPlaceholder('eg. 893982-T')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Registration No"
            value={props.data.COMREG}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMTEL')
                      props.setFormValue(props.data.COMTEL)
                      props.setOpen(true)
                      props.setFormTitle('Company Tel')
                      props.setFormPlaceholder('eg. 603-89821123')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Tel"
            value={props.data.COMTEL}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMFAX')
                      props.setFormValue(props.data.COMFAX)
                      props.setOpen(true)
                      props.setFormTitle('Company Fax')
                      props.setFormPlaceholder('eg. 603-89821123')
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Fax"
            value={props.data.COMFAX}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMEML')
                      props.setFormValue(props.data.COMEML)
                      props.setOpen(true)
                      props.setFormTitle('Company Email')
                      props.setFormPlaceholder(
                        'eg. user@ecommercecompany.com.my'
                      )
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Email"
            value={props.data.COMEML}
          />
          <br />
          <TextFieldDisplay
            fullWidth={false}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    color="primary"
                    aria-label="directions"
                    onClick={() => {
                      props.setFormKey('COMURL')
                      props.setFormValue(props.data.COMURL)
                      props.setOpen(true)
                      props.setFormTitle('Company Website')
                      props.setFormPlaceholder(
                        'eg. http://ecommercecompany.com.my'
                      )
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            label="Company Website"
            value={props.data.COMURL}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default ShopSettingsDisplay
