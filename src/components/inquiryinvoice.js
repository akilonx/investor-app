import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import TextFieldDisplay from './common/textFieldDisplay'

import { makeStyles } from '@material-ui/core/styles'
import { GET_CON } from './graphql/consignment'
import { SEARCH_INVOICE } from './graphql/billing'
import { useMutation, useQuery } from '@apollo/react-hooks'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import Loading from './common/loading'
import PrintIcon from '@material-ui/icons/Print'
import PodDialog from './despatching/podDialog'
import BackupIcon from '@material-ui/icons/Backup'
import Divider from '@material-ui/core/Divider'
import TableHalf from './common/tableHalf'
import CircularProgress from '@material-ui/core/CircularProgress'

import { green } from '@material-ui/core/colors'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  displayDiv: {
    background: theme.palette.background.paper,
    minHeight: '340px',
    margin: 20,
    marginTop: 30,
  },
  small: {
    width: 300,
  },
  big: {
    width: 800,
  },
  dialog: {
    boxShadow: '0 8px 6px -6px black',
    position: 'static',
    left: '20%',
    top: '10%',
  },
  table: {
    padding: 0,
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))

const tableHead = [
  {
    id: 'ConsignmentNo',
    numeric: false,
    disablePadding: true,
    label: 'CN',
    align: 'left',
  },
  {
    id: 'ConsignmentDestination',
    numeric: false,
    disablePadding: true,
    label: 'Dest',
    align: 'left',
  },
  {
    id: 'ETD',
    numeric: false,
    disablePadding: true,
    label: 'ETD',
    align: 'left',
  },
  {
    id: 'Pieces',
    numeric: false,
    disablePadding: true,
    label: 'Pieces',
    align: 'right',
  },
  {
    id: 'ETWeightD',
    numeric: false,
    disablePadding: true,
    label: 'Weight',
    align: 'right',
  },
  {
    id: 'Amount',
    numeric: false,
    disablePadding: true,
    label: 'Amount',
    align: 'right',
  },
]
export default function Inquiry(props) {
  const classes = useStyles()
  const [invoice, setInvoice] = useState({})
  const [selected, setSelected] = useState([])
  const [confirm, setConfirm] = useState(false)
  const [disable, setDisable] = useState(false)
  const {
    loading,
    data: { searchinvoice: billingheader } = { billingheader: [] },
    refetch,
  } = useQuery(SEARCH_INVOICE, {
    variables: { invoiceNo: props.location.pathname.split('/')[2] },
  })

  const [open, setOpen] = useState(false)
  const [openImage, setOpenImage] = useState(false)
  const [totalAmount, setTotalAmount] = useState(0)
  const [totalPieces, setTotalPieces] = useState(0)
  const [totalWeight, setTotalWeight] = useState(0)

  useEffect(() => {
    if (!billingheader) return

    const totalAmount = billingheader.reduce((a, b) => a + b.Amount, 0)
    setTotalAmount(totalAmount)

    const totalPieces = billingheader.reduce((a, b) => a + b.Pieces, 0)
    setTotalPieces(totalPieces)

    const totalWeight = billingheader.reduce((a, b) => a + b.Weight, 0)
    setTotalWeight(totalWeight)
  }, [billingheader])

  const sumAmount = (billings) => {
    const total = billings.reduce((a, b) => a + b.Amount, 0)
    return total
  }

  const handlePrintInvoice = () => {
    setDisable(true)
    axios
      .get(`${restApi}/invoicing/print/${billingheader[0].InvoiceNo}`, {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/pdf',
        },
      })
      .then((response) => {
        setDisable(false)
        /* const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute(
          'download',
          `ShipmentAdvice-${billingheader.ConsignmentNo}.pdf`
        ) //or any other extension
        document.body.appendChild(link)
        link.click() */
        var file = new Blob([response.data], { type: 'application/pdf' })
        var fileURL = URL.createObjectURL(file)
        window.open(fileURL, '_blank')
      })
      .catch((error) => console.log(error))
  }

  if (loading)
    return (
      <div style={{ padding: 20 }}>
        <Loading />
      </div>
    )

  if (!loading && billingheader && billingheader.length === 0)
    return (
      <div
        style={{
          padding: 20,
        }}
      >
        Consignment Not Found
      </div>
    )
  return (
    <div className={classes.displayDiv}>
      <Grid container spacing={3}>
        <Grid item md={8} xs={12} style={{ padding: 30, paddingTop: 10 }}>
          <Grid container spacing={3}>
            <Grid item md={12} xs={12}>
              <h2 style={{ marginBottom: 5 }}>Invoice</h2>
              <Divider />
            </Grid>
          </Grid>
          {billingheader && billingheader.length > 0 && (
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="Invoice No"
                  value={billingheader[0].InvoiceNo}
                />
                <TextFieldDisplay
                  label="Customer"
                  value={`${billingheader[0].InvoiceToCode} ${billingheader[0].InvoiceToName2}`}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <TextFieldDisplay
                  label="Invoice Date"
                  value={
                    billingheader[0].InvoiceDate &&
                    new Intl.DateTimeFormat('en-GB', {
                      hour: 'numeric',
                      minute: 'numeric',
                      year: 'numeric',
                      month: 'numeric',
                      day: 'numeric',
                      hour12: true,
                    }).format(new Date(billingheader[0].InvoiceDate))
                  }
                />
              </Grid>
            </Grid>
          )}

          {billingheader && billingheader.length > 0 && (
            <Grid container spacing={3}>
              <Grid item md={12} xs={12} style={{ paddingLeft: 12 }}>
                <br />
                <Divider />
                <Table
                  className={classes.table}
                  size="small"
                  aria-label="a dense table"
                >
                  <TableHead>
                    <TableRow>
                      {tableHead.map((header, index) => (
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: header.align,
                          }}
                          key={index}
                        >
                          {header.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {billingheader.map((row) => (
                      <TableRow key={row.id}>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                          component="th"
                          scope="row"
                        >
                          {row.ConsignmentNo}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                          component="th"
                          scope="row"
                        >
                          {row.ConsignmentDestination}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 5,
                            paddingBottom: 5,
                          }}
                          component="th"
                          scope="row"
                        >
                          {row.ETD &&
                            new Intl.DateTimeFormat('en-GB').format(
                              new Date(row.ETD)
                            )}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 5,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: 'right',
                          }}
                          component="th"
                          scope="row"
                        >
                          {row.Pieces}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 5,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: 'right',
                          }}
                          component="th"
                          scope="row"
                        >
                          {parseFloat(row.Weight).toFixed(1)}
                        </TableCell>
                        <TableCell
                          style={{
                            paddingLeft: 5,
                            paddingRight: 20,
                            paddingTop: 5,
                            paddingBottom: 5,
                            textAlign: 'right',
                          }}
                          component="th"
                          scope="row"
                        >
                          {parseFloat(sumAmount(row.BillingDetails)).toFixed(2)}
                        </TableCell>
                      </TableRow>
                    ))}
                    <TableRow key={10000000}>
                      <TableCell
                        style={{
                          paddingLeft: 0,
                          paddingRight: 0,
                          paddingTop: 5,
                          paddingBottom: 5,
                        }}
                        component="th"
                        scope="row"
                      >
                        <b>Total</b>
                      </TableCell>
                      <TableCell
                        style={{
                          paddingLeft: 0,
                          paddingRight: 0,
                          paddingTop: 5,
                          paddingBottom: 5,
                        }}
                        component="th"
                        scope="row"
                      ></TableCell>
                      <TableCell
                        style={{
                          paddingLeft: 0,
                          paddingRight: 0,
                          paddingTop: 5,
                          paddingBottom: 5,
                        }}
                        component="th"
                        scope="row"
                      ></TableCell>
                      <TableCell
                        style={{
                          paddingLeft: 5,
                          paddingRight: 20,
                          paddingTop: 5,
                          paddingBottom: 5,
                          textAlign: 'right',
                        }}
                        component="th"
                        scope="row"
                      >
                        <b>{parseFloat(totalPieces).toFixed(0)}</b>
                      </TableCell>
                      <TableCell
                        style={{
                          paddingLeft: 5,
                          paddingRight: 20,
                          paddingTop: 5,
                          paddingBottom: 5,
                          textAlign: 'right',
                        }}
                        component="th"
                        scope="row"
                      >
                        <b>{parseFloat(totalWeight).toFixed(1)}</b>
                      </TableCell>
                      <TableCell
                        style={{
                          paddingLeft: 5,
                          paddingRight: 20,
                          paddingTop: 5,
                          paddingBottom: 5,
                          textAlign: 'right',
                        }}
                        component="th"
                        scope="row"
                      >
                        <b>{parseFloat(totalAmount).toFixed(2)}</b>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                <Divider />
                <br /> <br /> <br /> <br /> <br />
              </Grid>
            </Grid>
          )}
        </Grid>

        <Grid
          item
          md={4}
          xs={12}
          style={{ background: '#ededed', minHeight: 350 }}
        >
          {billingheader && billingheader.length > 0 && (
            <div
              style={{
                paddingTop: 70,
                paddingLeft: 30,
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <div className={classes.wrapper}>
                <Button
                  variant="contained"
                  disableElevation
                  color="primary"
                  disabled={disable}
                  onClick={handlePrintInvoice}
                  startIcon={<PrintIcon />}
                >
                  Print Invoice
                </Button>
                {disable && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )}
              </div>
              <br />
            </div>
          )}
          {/* <pre>{JSON.stringify(billingheader, null, 4)}</pre> */}
        </Grid>
      </Grid>
    </div>
  )
}
