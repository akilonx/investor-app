import { gql } from 'apollo-boost'

export const GET_STAFFS = gql`
  {
    staffs {
      id
      StaffID
      StaffName
      Department
    }
  }
`
