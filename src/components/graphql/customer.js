import { gql } from 'apollo-boost'

export const GET_CUSTOMER_SEARCH_SELECT = gql`
  query SearchCustomers($CustomerType: String, $Search: String) {
    searchcustomers(CustomerType: $CustomerType, Search: $Search) {
      id
      CustomerCode
      CustomerName
      CoLoaderCode
    }
  }
`

export const GET_CUSTOMER_SELECT = gql`
  query Customers($CustomerType: String) {
    customers(CustomerType: $CustomerType) {
      id
      CustomerCode
      CustomerName
      CoLoaderCode
    }
  }
`

export const GET_CUSTOMER_SEARCH_LIMIT = gql`
  query CustomerSearchLimit {
    customersearchlimit {
      id
      CustomerCode
      CustomerName
      CoLoaderCode
    }
  }
`

/* export const GET_CONS = gql`
  query Consignment($limit: Int, $offset: Int) {
    consignments(limit: $limit, offset: $offset) {
      total
      data {
        id
        ConsignmentNo
        Pieces
        Weight
        Destination
        AirlineID
        AWB
        AWBID
        ShipperCode
      }
    }
  }
` */

export const GET_CUSTOMER_SEARCH = gql`
  mutation CustomerSearch($search: String!) {
    customersearch(search: $search) {
      id
      CustomerCode
      CustomerName
    }
  }
`

export const GET_CUSTOMER = gql`
  query Customer($id: ID!) {
    customer(id: $id) {
      id
      CustomerCode
      CustomerName
      CompanyCode
      PersonInCharge
      PersonInCharge2
      CustomerType
      CountryCode
      PaymentTermCode
      StaffID
      Address1
      Address2
      Address3
      Address4
      TelNo1
      TelNo2
      FaxNo1
      FaxNo2
      Email1
      Email2
      Remark
      Status
      ActiveQuotation
      CoLoaderCode
      CoLoaderName
      CustomerTypeName
      PaymentTermName
      CountryName
      UpdatedBy
      LastModified
      CreatedBy
      CreateOn
    }
  }
`
export const GET_CUSTOMERS = gql`
  query Customer($limit: Int, $offset: Int) {
    customerspagination(limit: $limit, offset: $offset) {
      total
      data {
        id
        CustomerCode
        CustomerName
        CompanyCode
        PersonInCharge
        PersonInCharge2
        CustomerType
        CountryCode
        PaymentTermCode
        StaffID
        Address1
        Address2
        Address3
        Address4
        TelNo1
        TelNo2
        FaxNo1
        FaxNo2
        Email1
        Email2
        Remark
        Status
        ActiveQuotation
        CoLoaderCode
        CoLoaderName
        CustomerTypeName
        PaymentTermName
        CountryName
        UpdatedBy
        LastModified
        CreatedBy
        CreateOn
      }
    }
  }
`

export const CREATE_CUSTOMER = gql`
  mutation CreateCustomer(
    $CustomerName: String
    $CompanyCode: String
    $PersonInCharge: String
    $PersonInCharge2: String
    $CustomerType: String
    $CountryCode: String
    $CoLoaderCode: String
    $PaymentTermCode: String
    $StaffID: String
    $Address1: String
    $Address2: String
    $Address3: String
    $Address4: String
    $TelNo1: String
    $TelNo2: String
    $FaxNo1: String
    $FaxNo2: String
    $Email1: String
    $Email2: String
    $Remark: String
  ) {
    createcustomer(
      CustomerName: $CustomerName
      CompanyCode: $CompanyCode
      PersonInCharge: $PersonInCharge
      PersonInCharge2: $PersonInCharge2
      CustomerType: $CustomerType
      CountryCode: $CountryCode
      CoLoaderCode: $CoLoaderCode
      PaymentTermCode: $PaymentTermCode
      StaffID: $StaffID
      Address1: $Address1
      Address2: $Address2
      Address3: $Address3
      Address4: $Address4
      TelNo1: $TelNo1
      TelNo2: $TelNo2
      FaxNo1: $FaxNo1
      FaxNo2: $FaxNo2
      Email1: $Email1
      Email2: $Email2
      Remark: $Remark
    ) {
      id
      CustomerCode
      CustomerName
      CoLoaderCode
    }
  }
`

export const DELETE_CUSTOMER = gql`
  mutation DeleteCustomer($id: ID!) {
    deletecustomer(id: $id)
  }
`

export const UPDATE_CUSTOMER = gql`
  mutation UpdateCustomer(
    $id: ID!
    $CustomerCode: String
    $CustomerName: String
    $CompanyCode: String
    $PersonInCharge: String
    $PersonInCharge2: String
    $CustomerType: String
    $CountryCode: String
    $CoLoaderCode: String
    $PaymentTermCode: String
    $StaffID: String
    $Address1: String
    $Address2: String
    $Address3: String
    $Address4: String
    $TelNo1: String
    $TelNo2: String
    $FaxNo1: String
    $FaxNo2: String
    $Email1: String
    $Email2: String
    $Remark: String
    $Status: String
  ) {
    updatecustomer(
      id: $id
      CustomerCode: $CustomerCode
      CustomerName: $CustomerName
      CompanyCode: $CompanyCode
      PersonInCharge: $PersonInCharge
      PersonInCharge2: $PersonInCharge2
      CustomerType: $CustomerType
      CountryCode: $CountryCode
      CoLoaderCode: $CoLoaderCode
      PaymentTermCode: $PaymentTermCode
      StaffID: $StaffID
      Address1: $Address1
      Address2: $Address2
      Address3: $Address3
      Address4: $Address4
      TelNo1: $TelNo1
      TelNo2: $TelNo2
      FaxNo1: $FaxNo1
      FaxNo2: $FaxNo2
      Email1: $Email1
      Email2: $Email2
      Remark: $Remark
      Status: $Status
    ) {
      id
      CustomerCode
      CustomerName
      CompanyCode
      PersonInCharge
      PersonInCharge2
      CustomerType
      CountryCode
      PaymentTermCode
      StaffID
      Address1
      Address2
      Address3
      Address4
      TelNo1
      TelNo2
      FaxNo1
      FaxNo2
      Email1
      Email2
      Remark
      Status
      ActiveQuotation
      CoLoaderCode
      CoLoaderName
      CustomerTypeName
      PaymentTermName
      CountryName
      UpdatedBy
      LastModified
      CreatedBy
      CreateOn
    }
  }
`
