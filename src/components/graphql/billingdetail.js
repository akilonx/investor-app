import { gql } from 'apollo-boost'

export const GET_BILLINGDETAILS = gql`
  query BillingDetails($BillingID: ID!) {
    billingdetails(BillingID: $BillingID) {
      id
      Title
      ItemCode
      ItemType
      Quantity
      UnitPrice
      Amount
    }
  }
`

export const DELETE_CHARGES = gql`
  mutation DeleteBillingDetails($id: ID!, $BillingID: ID!) {
    deletebillingdetails(id: $id, BillingID: $BillingID)
  }
`

export const CREATE_OTHERS = gql`
  mutation CreateOthers(
    $BillingID: ID
    $AWBNo: String
    $AWBID: Int
    $ConsignmentNo: String
    $ConsignmentID: Int
    $Title: String
    $ItemCode: String
    $ItemType: String
    $Quantity: String
    $UnitPrice: String
    $Amount: String
  ) {
    createothers(
      BillingID: $BillingID
      AWBNo: $AWBNo
      AWBID: $AWBID
      ConsignmentNo: $ConsignmentNo
      ConsignmentID: $ConsignmentID
      Title: $Title
      ItemCode: $ItemCode
      ItemType: $ItemType
      Quantity: $Quantity
      UnitPrice: $UnitPrice
      Amount: $Amount
    ) {
      id
      Title
      ItemCode
      ItemType
      Quantity
      UnitPrice
      Amount
    }
  }
`
