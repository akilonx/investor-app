import { gql } from 'apollo-boost'

export const GET_BILLINGHEADERS = gql`
  query BillingHeaders($awbId: ID!) {
    billingheaders(awbId: $awbId) {
      id
      AWBNo
      AWBID
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ShipperColoaderQuotationItemCode
      ShipperColoaderCode
      ShipperColoaderName
      ShipperPaymentTerm
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      ConsigneeColoaderQuotationItemCode
      ConsigneeColoaderCode
      ConsigneeColoaderName
      ConsigneePaymentTerm
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Pieces
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
    }
  }
`

export const GET_BILLINGHEADERSCOSTING = gql`
  query BillingHeadersCosting($awbId: ID!) {
    billingheaderscosting(awbId: $awbId) {
      id
      AWBNo
      AWBID
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ShipperColoaderQuotationItemCode
      ShipperColoaderCode
      ShipperColoaderName
      ShipperPaymentTerm
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      ConsigneeColoaderQuotationItemCode
      ConsigneeColoaderCode
      ConsigneeColoaderName
      ConsigneePaymentTerm
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Pieces
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
    }
  }
`

export const SEARCH_INVOICE = gql`
  query SearchInvoice($invoiceNo: String!) {
    searchinvoice(invoiceNo: $invoiceNo) {
      id
      AWBNo
      AWBID
      BillDate
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      CNOn
      Pieces
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceNo
      InvoiceDate
      InvoiceToName2
      InvoiceToCode
      Image
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      BillBy
      lastUpdatedBy
      BillingDetails {
        ItemCode
        Title
        Quantity
        UnitPrice
        Amount
        lastUpdatedBy
        lastUpdated
        createdAt
      }
    }
  }
`

export const SEARCH_BILLINGHEADER = gql`
  query SearchBillingHeader($id: ID!) {
    searchbillingheader(id: $id) {
      id
      AWBNo
      AWBID
      BillDate
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      CNOn
      Pieces
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceNo
      InvoiceDate
      InvoiceToName2
      InvoiceToCode
      Image
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      BillBy
      lastUpdatedBy
      lastUpdated
      MAWBNo
      BillingDetails {
        ItemCode
        Title
        Quantity
        UnitPrice
        Amount
        lastUpdatedBy
        lastUpdated
        createdAt
      }
    }
  }
`

export const GET_BILLINGHEADER = gql`
  query BillingHeader($id: ID!) {
    billingheader(id: $id) {
      id
      AWBNo
      AWBID
      BillDate
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      CNOn
      Pieces
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceNo
      InvoiceDate
      InvoiceToName2
      InvoiceToCode
      Image
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      BillBy
      lastUpdatedBy
      lastUpdated
      BillingDetails {
        ItemCode
        Title
        Quantity
        UnitPrice
        Amount
        lastUpdatedBy
        lastUpdated
        createdAt
      }
    }
  }
`

export const GET_BILLABLES = gql`
  {
    billables {
      id
      AWBNo
      AWBID
      AirlineID
      AgentName
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
      TotalWeight
      TotalAmount
      TotalPieces
      TotalConsignment
      ConsignmentList
      CoLoaderName
      MAWBNo
    }
  }
`

export const UPDATE_BILLINGHEADER = gql`
  mutation UpdateBilling(
    $id: ID!
    $ShipperCode: String
    $ConsigneeCode: String
    $InvoiceToCode: String
    $BillTo: String
    $PPorCC: String
    $Parcel: String
    $Remark: String
    $Weight: Float
  ) {
    updateBilling(
      id: $id
      ShipperCode: $ShipperCode
      ConsigneeCode: $ConsigneeCode
      InvoiceToCode: $InvoiceToCode
      BillTo: $BillTo
      PPorCC: $PPorCC
      Parcel: $Parcel
      Remark: $Remark
      Weight: $Weight
    ) {
      id
      AWBNo
      AWBID
      AirlineID
      AgentName
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ShipperColoaderQuotationItemCode
      ShipperColoaderCode
      ShipperColoaderName
      ShipperPaymentTerm
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      ConsigneeColoaderQuotationItemCode
      ConsigneeColoaderCode
      ConsigneeColoaderName
      ConsigneePaymentTerm
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
    }
  }
`

export const GET_BILLING_INVOICETO = gql`
  query BillingByInvoiceTo($InvoiceToCode: String!, $from: Date, $to: Date) {
    billingbyinvoiceto(InvoiceToCode: $InvoiceToCode, from: $from, to: $to) {
      id
      AWBNo
      AWBID
      AirlineID
      AgentName
      ConsignmentNo
      ConsignmentID
      ConsignmentDestination
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ShipperColoaderQuotationItemCode
      ShipperColoaderCode
      ShipperColoaderName
      ShipperPaymentTerm
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      ConsigneeColoaderQuotationItemCode
      ConsigneeColoaderCode
      ConsigneeColoaderName
      ConsigneePaymentTerm
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Pieces
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
    }
  }
`

export const GET_BILLING_GROUPBY_INVOICETO = gql`
  query BillingGroupByInvoiceTo($from: Date, $to: Date) {
    billinggroupbyinvoiceto(from: $from, to: $to) {
      id
      InvoiceToName2
      InvoiceToCode
      TotalConsignment
      TotalAmount
    }
  }
`

export const GET_REPORT_YEARS = gql`
  {
    reportyears {
      id
      name
    }
  }
`

export const UPDATE_PREALERT_STATUS = gql`
  mutation UpdatePrealertStatus($AWBID: Int) {
    updateprealert(AWBID: $AWBID) {
      id
      AWBNo
      AWBID
      ConsignmentNo
      ConsignmentID
      ShipperCode
      ShipperName
      ShipperQuotationNo
      ShipperQuotationItemCode
      ConsigneeName
      ConsigneeCode
      ConsigneeQuotationNo
      ConsigneeQuotationItemCode
      BillToName2
      BillTo
      BillToCode
      InvoiceToName2
      InvoiceToCode
      PPorCC
      Parcel
      Amount
      Weight
      Remark
      ETD
      ETA
      POD
      POL
    }
  }
`
