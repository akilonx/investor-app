import { gql } from 'apollo-boost'

export const GET_DRIVERS = gql`
  {
    drivers {
      id
      DriverNo
      DriverName
      ContactNo
      Username
      Password
      Active
    }
  }
`

export const REMOVE_DRIVER = gql`
  mutation RemoveDriver($id: ID!) {
    removedriver(id: $id)
  }
`

export const UPDATE_DRIVER = gql`
  mutation UpdateDriver(
    $id: ID!
    $DriverNo: String
    $DriverName: String
    $ContactNo: String
    $Password: String
  ) {
    updatedriver(
      id: $id
      DriverName: $DriverName
      DriverNo: $DriverNo
      ContactNo: $ContactNo
      Password: $Password
    ) {
      id
      DriverNo
      DriverName
      ContactNo
      Username
      Password
      Active
    }
  }
`

export const CREATE_DRIVER = gql`
  mutation CreateDriver(
    $DriverNo: String
    $DriverName: String
    $ContactNo: String
    $Password: String
  ) {
    createdriver(
      DriverName: $DriverName
      DriverNo: $DriverNo
      ContactNo: $ContactNo
      Password: $Password
    ) {
      id
      DriverNo
      DriverName
      ContactNo
      Username
      Password
      Active
    }
  }
`
