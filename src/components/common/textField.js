import React from 'react'

import { FormikTextField } from '@dccs/react-formik-mui'

const TextField = props => (
  <FormikTextField {...props} fullWidth autoComplete="off" />
)

export default TextField
