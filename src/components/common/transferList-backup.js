import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIosRounded'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIosRounded'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  cardHeader: {
    backgroundColor: theme.palette.primary.paper,
    padding: theme.spacing(1, 2),
  },
  cardHeaderTitle: {
    fontSize: 16,
  },
  list: {
    width: 500,
    height: 330,
    backgroundColor: theme.palette.primary.paper,
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}))

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1)
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1)
}

function union(a, b) {
  return [...a, ...not(b, a)]
}

export default function TransferList(props) {
  const classes = useStyles()
  const [checked, setChecked] = useState([])

  const leftChecked = intersection(checked, props.left)
  const rightChecked = intersection(checked, props.right)

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value)
    //const newChecked = [...checked]
    const newChecked = []

    if (currentIndex === -1) {
      newChecked.push(value)
    } else {
      newChecked.splice(currentIndex, 1)
    }

    setChecked(newChecked)
  }

  const numberOfChecked = (items) => intersection(checked, items).length

  const handleToggleAll = (items) => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items))
    } else {
      setChecked(union(checked, items))
    }
  }

  const handleCheckedRight = () => {
    //props.setRight(props.right.concat(leftChecked))
    props.right.concat(leftChecked).map((item) => {
      props.setRight({
        variables: {
          id: item.id,
          AWBID: props.selectedAwb,
        },
      })
    })

    /* not(props.left, leftChecked).map(item => {
      props.setLeft({
        variables: {
          id: item.id,
          AWBID: null
        }
      })
    }) */
    //props.setLeft(not(props.left, leftChecked))

    setChecked(not(checked, leftChecked))
  }

  const handleCheckedLeft = () => {
    console.log(rightChecked)
    if (rightChecked && rightChecked.length == 0) return
    /* props.left.concat(rightChecked).map(item => {
      props.setLeft({
        variables: {
          id: item.id,
          AWBID: props.selectedAwb
        }
      })
    })  */

    props.setLeft({
      variables: {
        id: rightChecked[0].id,
        AWBID: props.selectedAwb,
      },
    })

    //props.setLeft(props.left.concat(rightChecked))
    //props.setRight(not(props.right, rightChecked))
    setChecked(not(checked, rightChecked))
  }

  /* useEffect(() => {
    console.log(props)
  }, [props]) */

  const customList = (title, items) => (
    <Card>
      <CardHeader
        classes={{ root: classes.cardHeader, title: classes.cardHeaderTitle }}
        /* avatar={
          <Checkbox
            onClick={handleToggleAll(items)}
            checked={
              numberOfChecked(items) === items.length && items.length !== 0
            }
            indeterminate={
              numberOfChecked(items) !== items.length &&
              numberOfChecked(items) !== 0
            }
            disabled={items.length === 0}
            inputProps={{ 'aria-label': 'all items selected' }}
          />
        } */
        title={title}
        /* subheader={`${numberOfChecked(items)}/${items.length} selected`} */
      />
      <Divider />
      <List className={classes.list} dense component="div" role="list">
        {items.map((value) => {
          const labelId = `transfer-list-all-item-${value.id}-label`

          return (
            <ListItem
              key={value.id}
              role="listitem"
              margin="dense"
              button
              onClick={handleToggle(value)}
            >
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText
                style={{ width: 100 }}
                id={labelId}
                primary={value.ConsignmentNo}
                secondary={value.ShipperCode}
              />
              <Divider orientation="vertical" />
              <ListItemText
                style={{ width: 50 }}
                id={labelId}
                primary=""
                secondary={value.Destination}
              />
              <Divider orientation="vertical" />

              <ListItemText
                style={{ width: 50 }}
                id={labelId}
                primary=""
                secondary={`${value.Pieces}pcs`}
              />
              <Divider orientation="vertical" />
              <ListItemText
                style={{ width: 50 }}
                id={labelId}
                primary=""
                secondary={`${value.Weight}kg`}
              />
            </ListItem>
          )
        })}
        <ListItem />
      </List>
    </Card>
  )

  return (
    <Grid
      container
      spacing={2}
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>{customList('Consignment', props.left)}</Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Button
            variant="contained"
            disableElevation
            color="primary"
            className={classes.button}
            onClick={handleCheckedRight}
            disabled={!props.selectedAwb || leftChecked.length === 0}
            aria-label="move selected right"
          >
            <ArrowForwardIosIcon />
          </Button>
          <Button
            variant="contained"
            disableElevation
            color="primary"
            onClick={handleCheckedLeft}
            disabled={!props.selectedAwb || rightChecked.length === 0}
            aria-label="move selected left"
          >
            <ArrowBackIosIcon />
          </Button>
        </Grid>
      </Grid>
      <Grid item>{customList('AWB', props.right)}</Grid>
    </Grid>
  )
}
