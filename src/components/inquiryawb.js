import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import TextFieldDisplay from './common/textFieldDisplay'

import AppBar from '@material-ui/core/AppBar'
import { makeStyles } from '@material-ui/core/styles'
import { SEARCH_AWB_SINGLE } from './graphql/awb'
import { useMutation, useQuery } from '@apollo/react-hooks'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Button from '@material-ui/core/Button'
import Loading from './common/loading'
import PrintIcon from '@material-ui/icons/Print'
import PodDialog from './despatching/podDialog'
import BackupIcon from '@material-ui/icons/Backup'
import Divider from '@material-ui/core/Divider'
import TableHalf from './common/tableHalf'
import BillingManifest from './account/billingManifest'

const restApi = process.env.REACT_APP_API

const useStyles = makeStyles((theme) => ({
  displayDiv: {
    background: theme.palette.background.paper,
    padding: '20px',
    minHeight: '340px',
    margin: 30,
  },
  small: {
    width: 300,
  },
  big: {
    width: 800,
  },
  paper: {
    boxShadow: 'none',
    paddingBottom: 0,
  },
  dialog: {
    boxShadow: '0 8px 6px -6px black',
    position: 'static',
    left: '20%',
    top: '10%',
  },
}))

const tableHead = [
  {
    id: 'ConsignmentNo',
    numeric: false,
    disablePadding: true,
    label: 'CN No',
  },
  {
    id: 'Destination',
    numeric: false,
    disablePadding: true,
    label: 'Destination',
  },
  {
    id: 'Pieces',
    numeric: true,
    disablePadding: true,
    label: 'Pieces',
  },
  {
    id: 'Weight',
    numeric: true,
    weight: true,
    disablePadding: false,
    label: 'Weight',
  },
]

export default function InquiryAwb(props) {
  const classes = useStyles()
  const [table, setTable] = useState({})
  const [selected, setSelected] = useState([])
  const [confirm, setConfirm] = useState(false)
  const {
    loading,
    data: { searchawb: awb } = {
      awb: {},
    },
    refetch,
  } = useQuery(SEARCH_AWB_SINGLE, {
    variables: {
      AWBNo: props.location.pathname.split('/')[2],
    },
  })
  const [open, setOpen] = useState(false)
  const [openImage, setOpenImage] = useState(false)
  const [totalWeight, setTotalWeight] = useState(0)

  useEffect(() => {
    if (!awb) return
    if (awb.Consignments.length == 0) return

    const totalWeight = awb.Consignments.reduce((a, b) => a + b.Weight, 0)
    setTotalWeight(totalWeight)
  }, [awb])

  const handleShipmentAdvice = () => {
    axios
      .get(`${restApi}/billing/shipmentadvice/${awb.id}`, {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/pdf',
        },
      })
      .then((response) => {
        var file = new Blob([response.data], {
          type: 'application/pdf',
        })
        var fileURL = URL.createObjectURL(file)
        window.open(fileURL, '_blank')
      })
      .catch((error) => console.log(error))
  }

  const handlePrint = () => {
    axios
      .get(restApi + `/premanifest/awbreport/${awb.id}`, {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/pdf',
        },
      })
      .then((response) => {
        var file = new Blob([response.data], {
          type: 'application/pdf',
        })
        var fileURL = URL.createObjectURL(file)
        window.open(fileURL, '_blank')
      })
      .catch((error) => console.log(error))
  }

  if (loading)
    return (
      <div
        style={{
          padding: 20,
        }}
      >
        <Loading />
      </div>
    )

  if (!loading && !awb)
    return (
      <div
        style={{
          padding: 20,
        }}
      >
        AWB Not Found
      </div>
    )

  const awbFormat = (awb) =>
    (awb && awb.replace(/(\w{3})(\w{4})(\w{4})/, '$1-$2-$3')) || ''

  return (
    <div className={classes.displayDiv}>
      <Grid container spacing={3}>
        <Grid item md={8} xs={12}>
          <Grid container spacing={3}>
            <Grid item md={3} xs={12}></Grid>
            <Grid item md={9} xs={12}>
              <h2> Air Way Bill </h2>

              <Divider />

              <AppBar
                className={classes.paper}
                position="static"
                color="default"
              >
                <BillingManifest data={awb} />
              </AppBar>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item md={3} xs={12}></Grid>
            <Grid item md={4} xs={12}>
              <TextFieldDisplay label="MAWB" value={awbFormat(awb.MAWBNo)} />
              <TextFieldDisplay label="HAWB" value={awbFormat(awb.AWBNo)} />
              <TextFieldDisplay label="POL" value={awb.POL} />
              <TextFieldDisplay
                label="ETA"
                value={
                  awb.ETA &&
                  new Intl.DateTimeFormat('en-GB').format(new Date(awb.ETA))
                }
              />
              <TextFieldDisplay label="Invoice No" value={awb.InvoiceNo} />
              <TextFieldDisplay
                label="AWB By"
                value={
                  awb.LastModified &&
                  awb.ModifiedBy &&
                  awb.ModifiedBy +
                    ' ' +
                    new Intl.DateTimeFormat('en-GB', {
                      hour: 'numeric',
                      minute: 'numeric',
                      year: 'numeric',
                      month: 'numeric',
                      day: 'numeric',
                      hour12: true,
                    }).format(new Date(awb.LastModified))
                }
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextFieldDisplay label="Agent" value={awb.AgentName} />
              <TextFieldDisplay label="POD" value={awb.POD} />
              <TextFieldDisplay
                label="ETD"
                value={
                  awb.ETD &&
                  new Intl.DateTimeFormat('en-GB').format(new Date(awb.ETD))
                }
              />
              <TextFieldDisplay
                label="Invoice Date"
                value={
                  awb.InvoiceDate &&
                  new Intl.DateTimeFormat('en-GB').format(
                    new Date(awb.InvoiceDate)
                  )
                }
              />
              <TextFieldDisplay
                label="Billing By"
                value={
                  awb.BillBy &&
                  awb.BillDate &&
                  awb.BillBy +
                    ' ' +
                    new Intl.DateTimeFormat('en-GB', {
                      hour: 'numeric',
                      minute: 'numeric',
                      year: 'numeric',
                      month: 'numeric',
                      day: 'numeric',
                      hour12: true,
                    }).format(new Date(awb.BillDate))
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item md={3} xs={12}></Grid>
            <Grid item md={9} xs={12}>
              <br />
              <Table
                className={classes.table}
                size="small"
                aria-label="a dense table"
              >
                <TableHead>
                  <TableRow>
                    {tableHead.map((header, index) => (
                      <TableCell key={index}>{header.label}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {awb.Consignments.map((row) => (
                    <TableRow key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.ConsignmentNo}
                      </TableCell>
                      <TableCell>{row.Destination}</TableCell>
                      <TableCell>{row.Pieces}</TableCell>
                      <TableCell>{row.Weight.toFixed(1)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              {/* const tableHead = [
  {
    id: 'ConsignmentNo',
    numeric: false,
    disablePadding: true,
    label: 'CN No'
  },
  {
    id: 'Destination',
    numeric: false,
    disablePadding: true,
    label: 'Destination'
  },
  {
    id: 'Pieces',
    numeric: true,
    disablePadding: true,
    label: 'Pieces'
  },
  {
    id: 'Weight',
    numeric: true,
    weight: true,
    disablePadding: false,
    label: 'Weight'
  }
]

              <TableHalf
                size="small"
                hidePagination={true}
                hideChange={true}
                hideDelete={true}
                selected={selected}
                setSelected={setSelected}
                setOpen={setOpen}
                tableState={setTable}
                tableData={awb.Consignments}
                tableHead={tableHead}
              ></TableHalf> */}
              <div
                style={{
                  float: 'right',
                  width: 300,
                  textAlign: 'right',
                  paddingRight: 20,
                  marginTop: 20,
                }}
              >
                <b> Total Weight: {parseFloat(totalWeight).toFixed(1)} </b>
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={4} xs={12}>
          <div>
            <Button
              variant="contained"
              disableElevation
              style={{ marginTop: 20 }}
              color="primary"
              onClick={handlePrint}
              startIcon={<BackupIcon />}
            >
              Print Awb Report
            </Button>
          </div>
          {/* 
                <pre>{JSON.stringify(awb, null, 4)}</pre> */}
        </Grid>
      </Grid>
    </div>
  )
}
